# Democratització de la tecnologia
El fàcil accés de la ciutadania a la tecnologia, especialment la relativa a la connectivitat a Internet i a l'ús de telèfons intel·ligents, comporta una transformació de la nostra societat cap a una nova cultura digital. La democratització de la tecnologia, en termes d'abaratiment i facilitat de connectivitat, permeten la creació de nous serveis des de l'administració particularitzats a les necessitats de la seva ciutadania i accessibles des del telèfon sense necessitats presencials, ni paperassa. Aquesta cultura digital ha d'impregnar la generació de serveis a la ciutadania, atès que signifiquen un abaratiment dels costos de prestació i en faciliten l'accés.

L’administració ha de vetllar per la igualtat d’accés a aquests nous serveis, facilitant eines i  recursos que garanteixin el seu ús i aprofitament independentment del context socioeconòmic, gènere o capacitats diverses de la ciutadania.


## objectiu general de l'àmbit
* Posar a l’abast de tothom les innovacions tecnològiques que l'administració incorpora en la seva acció municipal, vetllat per un accés no discriminador a tràmits, serveis i accions digitals de l’administració.
* Treballar per disminuir la distància creada a partir dels diferents usos tecnològics per part dels diferents sectors de la societat (escletxa digital), creada tant per accés a la tecnologia com per coneixement. Acompanyar tota acció municipal que inclogui una vessant digital de la formació necessària.
* Promoure polítiques divulgatives i formatives que fomentin la visibilització de la diversitat de gènere en el mon tecnològic així com el foment de les vocacions tecnològiques en nenes i noies.
* Promoure una relació conscient i crítica de la ciutadania amb la tecnologia.

## Proposta programàtica
1. Posar en marxa programes de formació i capacitació digital per a la ciutadania, així com reforçar els existents.
1. Promoure espais de connexió i ús tecnològic que ofereixin eines i recursos de manera gratuïta, així com dotar de recursos als existents (biblioteques, centres cívics o telecentres).
1. Oferir programes pedagògics en coordinació amb centres educatius de diferents nivells que permetin abordar com treballar la tecnologia de manera inclusiva i respectuosa amb la ciutadania, introduint-hi la perspectiva de gènere.
1. Oferir formació tecnològica (inicial i avançada) a col·lectius amb risc d’exclusió social per motius socioeconòmics, en temàtiques d’innovació tecnològica, en coordinació amb els serveis socials municipals.
1. Garantir que el personal de l'administració podrà acompanyar a la ciutadania en la seva interacció digital amb l'administració a partir de programes de formació interna.

## Experiències inspiradores
Pendent


