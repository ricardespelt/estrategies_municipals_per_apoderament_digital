## Mesures per a l'apoderament digital als municipis

### Infraestructura de telecomunicacions
La infraestructura de telecomunicacions és tot aquell cablejat i maquinari que fa possible que la informació viatgi d'un lloc a un altre. És el pilar d'Internet. Actualment tenim infraestructures públiques, privades i procomunes, segons quin el seu propietari i quin model de gestió té.
Les decisions que es prenen relatives a la regulació, evolució o ampliació d'aquesta infraestructura han sigut objecte de fortes tensions entre diferents interessos privats que busquen controlar la xarxa i la  informació que hi circula per poder extreure'n un benefici. També hem pogut veure com el control d'aquesta xarxa s'ha utilitzat dins de campanyes de censura.


#### Objectius generals
* Tendir cap a la universalització i sobirania de les xarxes de telecomunicacions, potenciant l'ús neutre de les xarxes de telecomunicacions.
* Impulsar que el desplegament de serveis de telecomunicacions fixes i mòbils, existents i nous com el 5G, es faci a través d'una xarxa compartida pels diferents operadors i usuaris públics i privats, evitant la privatització de l'espai públic sense compartició.
* Potenciar les operadores de proximitat i de caràcter cooperatiu, evitant el monopoli de les grans operadores, incrementant-ne el seu control.
* Desplegar i consolidar una xarxa de dades d’Internet de les coses oberta, lliure i neutral, creada col·lectivament des de baix.


#### Proposta programàtica

##### Consolidació d'un ecosistema d'Infraestructura col·laborativa i oberta de tecnologies lliures
1. Proporcionar accés real a la ciutadania i a la societat en general a una oferta assequible i variada de serveis de telecomunicacions de la màxima qualitat, capacitat i neutralitat.
2. Promoure ordenances municipals de desplegament de nova infraestructura com la proposada per Guifi.net, que promou amb la màxima agilitat i eficiència possible, estimula i maximitza l'eficiència de qualsevol tipus d'inversió i, alhora assegura la seva sostenibilitat en base a l'ús que se'n fa, minimitzant el cost per a l'administració pública i també per al ciutadà i la societat en general.
3. Afavorir les xarxes comunals i obertes, incidint en la necessitat d'avaluació del impacte social i territorial dels desplegaments, tenint en compte els possibles casos d'ús.

##### Desplegament de noves infraestructures per serveis de comunicacions (existents o de nova generació com 5G i futurs)
4. Impulsar el desplegament d'infraestructures d'accés (fixe i mòbil, existent o nova com 5G) a través d'una xarxa compartida pels diferents operadors.
5. Facilitar la compartició d'infraestructura pública: implantació d'antenes o pas de cablejat en edificis i infraestructures públiques.

##### Contractació de serveis de telecomunicacions
6. Contractar serveis a companyies de telecomunicacions que no discriminin el trànsit, el filtrin o l'interrompin, com en el cas del referèndum d'autodeterminació de l'1 d'octubre de 2017.

##### Internet dels Objectes
7. Promoure les iniciatives de xarxes Internet dels objectes oberta, lliure i neutral.
8. Contribuir a una arquitectura d'Internet dels objectes per les ciutats que sigui oberta i interoperable.


### Programari Lliure

Quan un programa informàtic es pot utilitzar, modificar, compartir i estudiar, l'anomenem lliure. El programari lliure ens permet llavors resoldre necessitats informàtiques comunes d'una manera inclusiva, en la que diferents actors poden tenir interès proactiu en una solució sense haver de desenvolupar-la dues vegades o demanar a un tercer que ho faci per nosaltres. Evita així la dependència a tercers que tenen un control sobre el programa i possibilita verificar què és exactament el que fa el programa, tant per assegurar que fa el que ha de fer, com per assegurar que no està tractant les nostres dades de maneres indesitjades.

#### Objectiu generals

* Assolir la plena sobirania municipal respecte quin codi i quins algorismes s'executen, quines dades s'emmagatzemen i qui hi té accés. Fer el codi auditable per part de la ciutadania.
* Independitzar els ajuntaments de les estratègies comercials dels proveïdors, millorant la capacitat de decisió sobre quines tecnologies s'implanten alhora que es promou la innovació i l'enfortiment del teixit tecnològic local.
* Aconseguir solucions informàtiques millor adaptades a cada cas i més fàcils d'evolucionar, a un menor cost.
* Contribuir a fer sostenible el patrimoni digital que suposa el programari lliure.

#### Proposta programàtica

##### Relació amb la ciutadania
9. Garantir que totes les webs i aplicacions municipals funcionen sense cap desavantatge amb programari lliure.
10. Utilitzar només formats i protocols oberts, amb especificacions públiques i que disposin de bones implementacions lliures.
11. No incorporar a les webs municipals serveis de tercers basats en programari privatiu, o que permeten la recol·lecció de dades per part de qui ofereix el servei. En general, no distribuir programari privatiu per cap via.
12. No acceptar donacions de programari privatiu per part d'empreses privades, ni tampoc de maquinari que obligui a l'ús de programari privatiu per a un correcte funcionament. Aquest punt és especialment important en el camp de l'educació.
13. Oferir programari que funcioni amb la llengua pròpia de la ciutadania. Sistemes traduïts incloent missatges d'error, documentació, diccionaris i sistemes de parla.

##### Implantació de programari lliure a cada ajuntament
14. Establir un pla rigorós de migració a programari lliure de tots els serveis d'informació de l'ajuntament.
15. Migrar l'ofimàtica interna a programari lliure, amb els corresponents plans de formació.
16. Que amb diners públics només es desenvolupi codi públic: assegurar en els nous contractes que es publiqui el codi sota llicències lliures. Dissenyar les integracions amb components privatius per tal que siguin temporals i fàcilment substituïbles.
17. Implantar els serveis essencials en ordinadors als que els servidors públics tinguin accés com a mínim telemàtic.
18. No adquirir maquinari que no sigui plenament funcional sota *kernels* lliures, com per exemple Linux.
19. Migrar progressivament a programari lliure totes les aplicacions d'escriptori i el sistema operatiu dels ordinadors municipals.

##### Cooperació entre administracions i amb les comunitats de programari lliure
20. Crear mecanismes de coordinació entre administracions que permetin mancomunar el desenvolupament i desplegament de serveis.
21. Prioritzar la participació en projectes ja existents, procurant que les millores realitzades quedin incorporades al producte original. Respectar les pràctiques i els codis de conducta de les comunitats en que es participa.
22. Afavorir la reutilització de solucions mitjançant la correcta documentació i la publicitat dels projectes.


### Política de dades

Que el coneixement és poder és una cosa que hem sabut sempre. Avui dia, mitjançant dades tenim molta possibilitat de coneixement. Caldrà trobar maneres de fer que les dades estiguin protegides, respectant la privacitat dels ciutadans i ciutadanes. Per això, necessitem assegurar l'accés públic a les dades obertes i posar eines per evitar els monopolis privats de dades.

#### Objectiu general de l'àmbit
* Fer una administració transparent i propera a la ciutadania.
* Facilitar l'accés i explotació dels conjunts de dades públics.
* Fer créixer les col·leccions de dades de lliure accés.
* Garantir, amb criteris ètics, la privacitat de les dades de caràcter personal, administrant-les de manera més conscient, i reduint els riscos derivats de la seva explotació.


#### Proposta programàtica

##### Open data per defecte
23. Publicar sota llicències lliures i formats reutilitzables tota la informació pública generada o gestionada per l'administració local (des dels pressupostos municipals fins als temps dels semàfors, p.e.).
24. Estandarditzar els formats de les dades publicades entre diferents ajuntaments i administracions, facilitant així l'anàlisi creuat de les dades.
25. Publicar les dades crues generades per a la realització d'estudis finançats amb diners públics, de manera que es puguin comprovar els estudis o fer-ne derivats.
26. Promoure la reutilització d'aquestes dades, mitjançant una publicitat activa, una bona accessibilitat al web municipal, i l'especificació de les llicències.

##### Privacitat de les dades de caràcter personal
27. Administrar les dades de caràcter personal únicament dins de l'administració i seguint estrictes criteris de seguretat, garantint els mitjans tècnics i els coneixements per gestionar-les sense dependre de tercers.
28. Limitar la recollida de dades de caràcter personal a aquells casos en què prèviament s'han establert les finalitats concretes per a les que seran utilitzades, minimitzant, així, la quantitat de dades personals que es recullen.
29. Implementar mecanismes per donar un màxim de serveis per conèixer, corregir o esborrar dades personals dels sistemes de l'administració; i no només el mínim legal que exigeix la GDPR.

### Democratització de la tecnologia
El fàcil accés de la ciutadania a la tecnologia, especialment la relativa a la connectivitat a Internet i a l'ús de telèfons intel·ligents, comporta una transformació de la nostra societat cap a una nova cultura digital. La democratització de la tecnologia, en termes d'abaratiment i facilitat de connectivitat, permeten la creació de nous serveis des de l'administració particularitzats a les necessitats de la seva ciutadania i accessibles des del telèfon sense necessitats presencials, ni paperassa. Aquesta cultura digital ha d'impregnar la generació de serveis a la ciutadania, atès que signifiquen un abaratiment dels costos de prestació i en faciliten l'accés.

L’administració ha de vetllar per la igualtat d’accés a aquests nous serveis, facilitant eines i  recursos que garanteixin el seu ús i aprofitament independentment del context socioeconòmic, gènere o capacitats diverses de la ciutadania.


#### Objectiu general de l'àmbit
* Posar a l’abast de tothom les innovacions tecnològiques que l'administració incorpora en la seva acció municipal, vetllat per un accés no discriminador a tràmits, serveis i accions digitals de l’administració.
* Treballar per disminuir la distància creada a partir dels diferents usos tecnològics per part dels diferents sectors de la societat (escletxa digital), creada tant per accés a la tecnologia com per coneixement. Acompanyar tota acció municipal que inclogui una vessant digital de la formació necessària.
* Promoure polítiques divulgatives i formatives que fomentin la visibilització de la diversitat de gènere en el mon tecnològic així com el foment de les vocacions tecnològiques en nenes i noies.
* Promoure una relació conscient i crítica de la ciutadania amb la tecnologia.

#### Proposta programàtica
30. Posar en marxa programes de formació i capacitació digital per a la ciutadania, així com reforçar els existents.
31. Promoure espais de connexió i ús tecnològic que ofereixin eines i recursos de manera gratuïta, així com dotar de recursos als existents (biblioteques, centres cívics o telecentres).
32. Oferir programes pedagògics en coordinació amb centres educatius de diferents nivells que permetin abordar com treballar la tecnologia de manera inclusiva i respectuosa amb la ciutadania, introduint-hi la perspectiva de gènere.
33. Oferir formació tecnològica (inicial i avançada) a col·lectius amb risc d’exclusió social per motius socioeconòmics, en temàtiques d’innovació tecnològica, en coordinació amb els serveis socials municipals.
34. Garantir que el personal de l'administració podrà acompanyar a la ciutadania en la seva interacció digital amb l'administració a partir de programes de formació interna.
35. Garantir l'accessibilitat a persones amb discapacitats. Cal que els formats de representació d'informació compleixin els estàndards i recomanacions quant a la representació d'informació per a persones amb capacitats diverses.
36. Cal garantir que models i algorismes, especialment en el context de l'aprenentatge automàtic, no introdueixen esbiaix que perjudiquin a les persones, com per exemple sexe, raça, religió o orientació sexual.

### Compra pública de dispositius electrònics i circularitat
En el moment de determinar les condicions de compra pública d'un dispositiu electrònic és quan es té la possibilitat de influir en les condicions de fabricació del producte però també en determinar les condicions per la reutilizació d'aquest un cop es deixi de fer servir, per esgotar del tot la seva vida útil, i garantir el reciclatge final (economia circular).

#### Objectiu general de l'àmbit
 * Compra de dispositius electrònics que minimitzi l’impacte negatiu en les persones i el medi ambient.

#### Proposta programàtica
37. Promoció de la reducció de l’impacte medi-ambiental i l’economia social i solidaria.
38. Respecte als drets laborals de les persones treballadores involucrades en la fabricació, manteniment, recollida i reciclatge de dispositius electrònics.
39. Compra pública responsable que respecti els drets laborals en la producció de béns electrònics.
40. Traçabilitat en origen dels dispositius per assegurar la circularitat: reparació, renovació, reutilització, garantir el reciclatge final i la transparència de les dades sobre aquests processos.
41. Compromís de donació dels dispositius a entitats socials al final del seu us, per allargar la vida útil dels dispositius i crear beneficis socials amb la reutilització.
42. Compra pública amb extensió de garantia que inclogui reparació i manteniment durant tot el periode d'utilització, per facilitar estendre la vida útil dels dispositius.
43. Promoció del coneixement sobre manteniment i reparació dels dispositius a les entitat públiques i els centres educatius (escoles, universitats).


### Estàndards lliures
Els documents, dades i interacció entre els serveis municipals i les persones son una necessitat i un recurs per a la ciutadania per molts aspectes de la seva vida. El format i les eines d'accés no han de ser mai obstacle.
Les especificacions i formats lliures estan totalment documentats i estan disponible públicament; han de ser lliures de restriccions de drets d'autor, reclamacions de propietat intel·lectual o llicències restrictives; el format es decideix per una organització de normes independent d'un proveïdor o per a una comunitat (p. ex., comunitat de desenvolupament de codi obert); hi han implementacions de eines de suport de lliure us.

#### Objectius generals
 * Facilitar la interoperabilitat i compartició de les dades en els serveis finançats amb fons públics pel benefici de la ciutadania, evitant barreres d'accés, fent servir especificacions per formats de dades i serveis que segueixin estàndards oberts.
 * Evitar crear dependències i costos de tercers: preferència per estàndards de formats de dades i protocols lliures de patents, llicències, drets intel·lectuals.
 * Evitar condicionar decisions amb la preferència per protocols lliures, àmpliament implementats i aplicats en diversitat de productes i serveis.

#### Proposta programàtica
44. Assegurar la interoperativitat: utilitzar formats oberts de fitxers com a eina per garantir l'accés a la informació sense obstacles i la bona interacció amb i entre els sistemes públics. A més a més, els estàndards que s'utilitzin han de tenir implementacions obertes que es verifiqui funcionin amb una varietat de dispositius o programari, inclouent sempre una opció de programari lliure.
45. Un dels requisits alhora de comprar aparells tecnològics ha de ser que aquests utilitzin estàndards lliures per realitzar la seva funció. D'aquesta manera s'eliminen les dependències cap a un sol fabricant.
46. Producció digital utilitzant llicències copyleft i Creative Commons que facilitin la reutilització de les obres artístiques i produccions dels/de les creadors/es locals finançades amb fons públics, com a eines de cultura lliure.
47. Establir polítiques actives d’ús d’estàndards oberts i de migració a programari lliure, havent establert una llista d’aplicacions homologades per a cada ús i funcionalitat requerides pel sector públic, i habilitar estratègies de reutilització, col·laboració i compartició d’esforços amb altres entitats públiques del món.
