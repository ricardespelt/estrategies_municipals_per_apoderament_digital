| Municipi | Candidatura | Persona de referencia | Comentaris |
| -------- | -------- | -------- |  -------- |
| Badalona | Guanyem Badalona en Comú | Aleix | es van disculpar per no haver vingut a la presentació i vam quedar que els tornariem a contactar nosaltres |
| Barcelona | CUP-Capgirem Barcelona  | Muriel | ens van convidar al debat sobre aquest tema d'on havien de sortir les propostes per actualitzar el programa i el grup de treball volia proposar incorporar-les en bloc |
| Barcelona | Barcelona en Comú | Guillem | estan interessats en afegir les propostes, estem pendents que ens convoguin a una reunió de cara a finals de gener |
| L'Hospitalet | CUP | Javier (sobtec) | |
| L'Hospitalet | Canviem L'Hospitalet | Muriel | encara no han tancat la votació del seu programa |
| Alella | Gent d'Alella | Muriel | s'ho van apuntar per incorporar-ho al seu programa |
| Lleida | El Comú de Lleida | Muriel | s'ho van apuntar per incorporar-ho al seu programa |
| Flix | Crida Flixanca | Muriel | Volen parlar |
| Espluga de Francolí | ERC | Muriel | Volen parlar |
| Viladecans | Podem* | Muriel | Volen xerrada |
| Olot | CUP* | Muriel | Hi vam fer xerrada |

Altres contactes
- **CUP ambit Nacional**

 La CUP està revisant el seu programa marc que serveix de referència per redactar els programes polítics a les diferents candidatures municipals en les que formarà part. S'està mirant de que les nostres propostes hi tinguin cabuda en aquest document.

 persona de referència: Franchi

- **Catalunya en Comú**

- **Pirates de Catalunya**

persona de referència: Muriel
